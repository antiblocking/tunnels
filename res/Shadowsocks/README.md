# [Shadowsock](https://shadowsocks.org/en/download/clients.html)
Китайский прозрачный прокси клиент и сервер.  С включенным [simple-obfs](https://github.com/shadowsocks/simple-obfs) транспортом обходит Китайский Фаервол. Будет работать для тех, кто развернул свои сервера. Мы делали это на DigitalOcean. Вы можете использовать любой доступный для вас сервер. В этом примере мы использовали HTTP обфускацию.

## [Разворачиваем сервер на Ubuntu 20.04 @ Digital Ocean](https://github.com/shadowsocks/shadowsocks-libev)
1. Идем на do.co/radiot
2. Регистрируемся и получаем 100$ на 60 дней.
3. Создаем Ubuntu 20.04 сервер за 5$
4. Подключаемся к серверу по ssh
5. Устанавливаем сервер и плагин для обфускации
```
apt update -yq
apt install -yq shadowsocks-libev simple-obfs
```
6. Конфигурируем сервер
`vi /etc/shadowsocks-libev/config-obfs.json`

Ставим пароль по усмотрению. В примере используется пароль MakeAsFree. Не забываем поменять поле server на 0.0.0.0.
```json
{
    "server":"0.0.0.0",
    "server_port":80,
    "local_port":1080,
    "password":"MakeAsFree",
    "timeout":60,
    "method":"chacha20-ietf-poly1305",
    "mode":"tcp_and_udp",
    "fast_open":true,
    "plugin":"obfs-server",
    "plugin_opts":"obfs=http;failover=127.0.0.1:8443;fast-open=true"
}
```
7. Запускаем сервис `systemctl enable --now shadowsocks-libev-server@config-obfs`
8. Проверить успешность запущенного сервера можно командой `systemctl status shadowsocks-libev-server@config-obfs`
```
● shadowsocks-libev-server@config-obfs.service - Shadowsocks-Libev Custom Server Service for config/obfs
     Loaded: loaded (/lib/systemd/system/shadowsocks-libev-server@.service; enabled; vendor preset: enabled)
     Active: active (running) since Thu 2020-08-13 01:57:14 UTC; 33min ago
       Docs: man:ss-server(1)
   Main PID: 57338 (ss-server)
      Tasks: 2 (limit: 1137)
     Memory: 4.6M
     CGroup: /system.slice/system-shadowsocks\x2dlibev\x2dserver.slice/shadowsocks-libev-server@config-obfs.service
             ├─57338 /usr/bin/ss-server -c /etc/shadowsocks-libev/config-obfs.json
             └─57349 obfs-server --fast-open
```
`Active: active (running)` - все в порядке.


## Android клиент
### Настраиваем клиент сами

1. Устанавливаем [OBFS Simple](https://play.google.com/store/apps/details?id=com.github.shadowsocks.plugin.obfs_local) плагин для Android
2. Устанавливаем из любого источника:
[Android, APK](https://github.com/Jigsaw-Code/outline-releases/blob/master/client/Outline.apk?raw=true)
[Android, F-Droid](https://f-droid.org/en/packages/com.gitlab.mahc9kez.shadowsocks.foss/)
[Android, Google Play](https://play.google.com/store/apps/details?id=com.github.shadowsocks)
3. Конфигурируем добавляем сервер на клиенте.
    - Кликаем по значку с "+"
    - "Manual Settings"

![Manual Settings](img/1.png)

4. Указываем IP адрес вашего сервера и порт из конфигурации. Я использовал 80.

![IP and Port](img/2.png)

5. Выбирае внизу используемый плагин

![Plugin](img/3.png)

6. Конфигурируем плагин.
7. Используемый метод обфускации: HTTP
8. Доменное имя: belta.by. Стоит попробовать другие доменны имена, если не сработало.

![Plugin Configuration](img/4.png)

9. Подключаемся
Нажимаем по круглой кнопке со значком бумажного самолетика.

![Connect](img/5.png)

### Добавляем сервер через ссылку
Если для вас уже кто-то отправил конфигурационную ссылку со схемой ss://, то сконфигурировать клиент будет проще.


1. Устанавливаем [OBFS Simple](https://play.google.com/store/apps/details?id=com.github.shadowsocks.plugin.obfs_local) плагин для Android
2. Устанавливаем из любого источника:
[Android, APK](https://github.com/Jigsaw-Code/outline-releases/blob/master/client/Outline.apk?raw=true)
[Android, F-Droid](https://f-droid.org/en/packages/com.gitlab.mahc9kez.shadowsocks.foss/)
[Android, Google Play](https://play.google.com/store/apps/details?id=com.github.shadowsocks)
3. Конфигурируем добавляем сервер на клиенте.
    - Кликаем по значку с "+"
    - "Import From Clipboard"

![Import from Clipboard](img/1.png)

4. Подключаемся
Нажимаем по круглой кнопке со значком бумажного самолетика.

![Connect](img/5.png)


## Linux, Ubuntu

1. Устанавливаем необходимые пакеты
```sh
sudo apt update -yq
sudo apt install -yq shadowsocks-libev obfs-simple
```

2. Конфигурируем сервер
```
sudo cp /etc/shadowsocks-libev/config-obfs.json /etc/shadowsocks-libev/client1.json
sudo vim /etc/shadowsocks-libev/client1.json
```

Моя конфигурация выглядит следующим образом
```json
{
    "server":"<your server address>",
    "server_port":80,
    "local_port":1080,
    "password":"MakeAsFree",
    "timeout":60,
    "method":"chacha20-ietf-poly1305",
    "mode":"tcp_and_udp",
    "fast_open":true,
    "plugin":"obfs-local",
    "plugin_opts":"obfs=http;failover=127.0.0.1:8443;obfs-host=www.belta.by;fast-open"
}

```

3. Запускаем прокси клиент

```sh
sudo systemctl enable --now shadowsocks-libev-local@client1
```

4. Проверяем работоспособность

```sh
$ sudo systemctl status shadowsocks-libev-local@client1
● shadowsocks-libev-local@client1.service - Shadowsocks-Libev Custom Client Service for client1
     Loaded: loaded (/lib/systemd/system/shadowsocks-libev-local@.service; disabled; vendor preset: enabled)
     Active: active (running) since Wed 2020-08-12 22:01:17 PDT; 2s ago
       Docs: man:ss-local(1)
   Main PID: 188940 (ss-local)
      Tasks: 2 (limit: 28457)
     Memory: 872.0K
     CGroup: /system.slice/system-shadowsocks\x2dlibev\x2dlocal.slice/shadowsocks-libev-local@client1.service
             ├─188940 /usr/bin/ss-local -c /etc/shadowsocks-libev/client1.json
             └─188942 obfs-local --fast-open

Aug 12 22:01:17 localhost ss-local[188940]:  2020-08-12 22:01:17 INFO: plugin "obfs-local" enabled
Aug 12 22:01:17 localhost ss-local[188940]:  2020-08-12 22:01:17 INFO: using tcp fast open
Aug 12 22:01:17 localhost ss-local[188940]:  2020-08-12 22:01:17 INFO: initializing ciphers... chacha20-ietf-poly1305
Aug 12 22:01:17 localhost ss-local[188940]:  2020-08-12 22:01:17 INFO: listening at 127.0.0.1:1080
Aug 12 22:01:17 localhost ss-local[188940]:  2020-08-12 22:01:17 INFO: udprelay enabled
Aug 12 22:01:17 localhost ss-local[188942]:  2020-08-12 22:01:17 [simple-obfs] INFO: using tcp fast open
Aug 12 22:01:17 localhost ss-local[188942]:  2020-08-12 22:01:17 [simple-obfs] INFO: obfuscating enabled
Aug 12 22:01:17 localhost ss-local[188942]:  2020-08-12 22:01:17 [simple-obfs] INFO: obfuscating hostname: www.belta.by
Aug 12 22:01:17 localhost ss-local[188942]:  2020-08-12 22:01:17 [simple-obfs] INFO: tcp port reuse enabled
Aug 12 22:01:17 localhost ss-local[188942]:  2020-08-12 22:01:17 [simple-obfs] INFO: listening at 127.0.0.1:41283
```
5. Настраиваем SOCKS прокси сервер `localhost:1080` в вашем приложении (Telegram, Firefox, Chromium).

6. Если пользоаватель вы продвинутый, то можете установить tun2socks из [BadVPN](https://github.com/ambrop72/badvpn), чтобы весь трафик прозрачно проксировать через tuntap интерфейс.

## Ссылки для скачивания
- [Windows](https://raw.githubusercontent.com/Jigsaw-Code/outline-releases/master/client/Outline-Client.exe)
- [Windows, OBFS Plugin](https://github.com/imgk/simple-obfs-Cygwin/releases)
- [Linux Ubuntu, CLI](https://packages.ubuntu.com/focal/shadowsocks-libev)
- [Linux, Ubuntu, OBFS Plugin](https://packages.ubuntu.com/focal/simple-obfs)
- [Android, APK](https://github.com/Jigsaw-Code/outline-releases/blob/master/client/Outline.apk?raw=true)
- [Android, F-Droid](https://f-droid.org/en/packages/com.gitlab.mahc9kez.shadowsocks.foss/)
- [Android, Google Play](https://play.google.com/store/apps/details?id=com.github.shadowsocks)
- [Android, OBFS Plugin](https://play.google.com/store/apps/details?id=com.github.shadowsocks.plugin.obfs_local)
- [macOS](https://itunes.apple.com/app/outline-app/id1356178125)
- [macOS, OBFS Plugin](https://formulae.brew.sh/formula/simple-obfs)
- [iOS](https://apps.apple.com/us/app/potatso-lite/id1239860606)