const fs = require("fs")
const path = require("path")
const showdown = require("showdown")


function listFiles(dirPath, arrayOfFiles) {
  const files = fs.readdirSync(dirPath)
  arrayOfFiles = arrayOfFiles || []

  files.forEach(function(file) {
    if (!file.startsWith('.')) {
      if (fs.statSync(dirPath + "/" + file).isDirectory()) {
        arrayOfFiles = listFiles(dirPath + "/" + file, arrayOfFiles)
      } else {
        arrayOfFiles.push(path.join(dirPath, "/", file))
      }
    }
  })

  return arrayOfFiles
}

const files = listFiles("res")

process.stdout.write(`
<!doctype html>
<html class="no-js" lang="">

<head>
  <meta charset="utf-8">
  <title></title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <meta property="og:title" content="">
  <meta property="og:type" content="">
  <meta property="og:url" content="">
  <meta property="og:image" content="">

  <meta name="theme-color" content="#fafafa">
</head>

<body>
<ul>
`)

for (let file of files) {
  if (file.endsWith('.md')) {
      const converter = new showdown.Converter()
      const md = fs.readFileSync(file).toString()
      file = file.replace(/\.md$/gi, '.html')
      fs.writeFileSync(file, converter.makeHtml(md))
  }
  process.stdout.write(`<li><a href="${file}">${file}</a></li>\n`)
}

process.stdout.write(`
</ul>
</body>
</html>
`)